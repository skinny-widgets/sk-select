
import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';


export class SkSelectImpl extends SkComponentImpl {

    get suffix() {
        return 'select';
    }

    beforeRendered() {
        super.beforeRendered();
        this.saveState();
    }

    saveState() {
        if (! this.comp.contentsState) {
            if (this.comp.useShadowRoot) {
                this.comp.contentsState = this.comp.el.host.innerHTML;
                //this.comp.el.host.innerHTML = '';
            } else {
                this.comp.contentsState = this.comp.el.innerHTML;
                //this.comp.el.innerHTML = '';
            }
        }
    }

    get body() {
        if (! this._body) {
            this._body = this.comp.el.querySelector('select');
        }
        return this._body;
    }

    set body(el) {
        this._body = el;
    }

    get subEls() {
        return [ 'body' ];
    }

    dumpState() {
        return {
            'contents': this.comp.contentsState
        }
    }

    restoreState(state) {
        //this.body.innerHTML = '';
        this.comp.el.insertAdjacentHTML('beforeend', state.contentsState);
        this.body.value = this.comp.value;
    }

    bindEvents() {
        this.body.onchange = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            if (this.comp.useShadowRoot) {
                this.comp.el.host.setAttribute('value', event.target.value);
                this.comp.el.host.dispatchEvent(new CustomEvent('change', { target: this.comp, bubbles: true, composed: true }));
                this.comp.el.host.dispatchEvent(new CustomEvent('skchange', { target: this.comp, bubbles: true, composed: true }));
            } else {
                this.comp.el.setAttribute('value', event.target.value);
                this.comp.el.dispatchEvent(new CustomEvent('change', { target: this.comp, bubbles: true, composed: true }));
                this.comp.el.dispatchEvent(new CustomEvent('skchange', { target: this.comp, bubbles: true, composed: true }));
            }
            this.comp.setAttribute('value', event.target.value);
            let result = this.validateWithAttrs();
            if (typeof result === 'string') {
                this.showInvalid();
            }
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
    }

    fitPopup() {
        if (this.comp.hasAttribute('open')) {
            let box = this.comp.el.host.getBoundingClientRect();
            this.popupEl.style.position = 'fixed';
            this.popupEl.style.top = `${(window.pageYOffset + box.bottom) - this.getScrollTop()}px`;
            this.popupEl.style.left = `${box.left}px`;
        }
    }

    bindFitPopup() {
        if (this.onScrollHandler) {
            window.removeEventListener('scroll', this.onScrollHandler);
        }
        this.onScrollHandler = this.fitPopup.bind(this);
        window.addEventListener('scroll', this.onScrollHandler);
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
        if (this.comp.useShadowRoot) {
            if (this.comp.value || this.comp.hasAttribute('value')) {
                this.body.value = this.comp.value || this.comp.getAttribute('value');
            }
        } else {
            if (this.comp.el.value) {
                this.body.value = this.comp.el.value;
            }
        }
    }

    enable() {
        super.enable();
    }

    disable() {
        super.disable();
    }

    get optionEls() {
        return (this.body && typeof this.body['querySelectorAll'] === 'function') ? this.body.querySelectorAll('option') : [];
    }
    
    selectOptionByValue(value) {
        if (! value) {
            this.selectOption(value);
        } else {
            if (this.options && Object.keys(this.options).length > 0) {
                for (let optionKey of Object.keys(this.options)) {
                    let option = this.options[optionKey];
                    if (value === option.value || value === option.innerHTML) {
                        this.selectOption(option);
                    }
                }
            } else {
                let options = this.optionEls;
                if (options) {
                    for (let option of options) {
                        if (value === option.value || value === option.innerHTML) {
                            this.selectOption(option);
                        }
                    }
                }
            }
        }
    }

    isClickedOnDropdown(event) {
        let xy = this.comp.hasAttribute('multiple')
            ? this.btnMultiselectEl?.getBoundingClientRect() : this.dropdownEl?.getBoundingClientRect();
        if (xy) {
            return (event.pageX > xy.left && event.pageX < (window.pageXOffset + xy.left + xy.width)
                && event.pageY > xy.top && event.pageY < (window.pageYOffset + xy.top + xy.height));
        } else {
            return false;
        }
    }
}