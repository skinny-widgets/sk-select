
import { SkElement } from '../../sk-core/src/sk-element.js';


export class SkSelect extends SkElement {

    get cnSuffix() {
        return 'select';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    get valueType() {
        return this.getAttribute('value-type');
    }

    set valueType(valueType) {
        this.setAttribute('value-type', valueType);
    }

    set value(value) {
        if (this.valueType === 'native') {
            if (Array.isArray(value)) {
                if (value[0]) {
                    this.setAttribute('value', value[0]);
                } else {
                    this.removeAttribute('value');
                }
            } else if (typeof value === 'object') {
                let keys = Object.keys(value);
                if (value[keys[0]]) {
                    this.setAttribute('value', value[keys[0]]);
                } else {
                    this.removeAttribute('value');
                }
            }
        } else {
            if (value === null) {
                this.removeAttribute('value');
            } else {
                this.setAttribute('value', value);
            }
        }
    }

    get value() {
        return this.getAttribute('value');
    }

    get subEls() {
        return [ 'body' ];
    }

    get body() {
        if (! this._body) {
            this._body = this.el.querySelector('select');
        }
        return this._body;
    }

    set body(el) {
        this._body = el;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'disabled' && oldValue !== newValue) {
            if (newValue) {
                this.impl.disable();
            } else {
                this.impl.enable();
            }
        }
        if (name === 'form-invalid') {
            if (newValue !== null) {
                this.impl.showInvalid();
            } else {
                this.impl.showValid();
            }
        }
        if (name === 'value' && oldValue !== newValue) {
            if (this.implRenderTimest) {
                this.impl.selectOptionByValue.call(this.impl, newValue);
            }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
    }

    get validity() {
        if (this.hasAttribute('required')) {
            let value = this.getAttribute('value');
            if (! value || value === '') {
                return { valid: false, valueMissing: true };
            }
        }
        return { valid: true };
    }

    flushValidity() {
        this.impl.flushValidation();
    }

    get selectedValues() {
        if (! this._selectedValues) {
            let valuesRaw = this.getAttribute('value');
            try {
                let values = JSON.parse(valuesRaw);
                this._selectedValues = values !== null ? values : [];
            } catch {
                this._selectedValues = (valuesRaw !== null && valuesRaw !== '') ? [ valuesRaw ] : [];
            }
        }
        this.value = this._selectedValues;
        return this._selectedValues;
    }

    set selectedValues(values) {
        this._selectedValues = values;
        this.value = values;
    }

    selectValue(value) {
        let values = this.selectedValues;
        if (Array.isArray(values) && ! values.find((item) => item === value)) {
            values.push(value);
        } else {
            values[value] = value;
        }
        this.selectedValues = values;
    }

    selectedOptions() {
        return this.body ? this.body.selectedOptions : [];
    }

    isSelected(value) {
        let values = this.selectedValues;
        if (Array.isArray(values)) {
            return (values.find((item) => item === value))
        } else if (values != null && value !== undefined && value !== '') {
            return (values[value]);
        } else {
            return false;
        }
    }

    deSelectValue(value) {
        let values = this.selectedValues;
        if (Array.isArray(values)) {
            values = values.filter(item => item !== value);
        } else {
            delete values[value];
        }
        this.selectedValues = values;
    }

    static get observedAttributes() {
        return [ 'disabled', 'required', 'form-invalid', 'value' ];
    }

}
